from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from faculty.models import*
from faculty.serializers import*

# Create your views here.

class FacultyListView(APIView):

	def get(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})

		faculties = Faculty.objects.filter(active=True).order_by('faculty_id')
		serializer = FacultySerializer(faculties,many=True)
		return Response(serializer.data)