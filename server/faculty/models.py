from django.db import models
from django.contrib.auth.models import User
from utility.models import*
from course.models import*

# Create your models here.

class Faculty(models.Model):

	# DATABASE FIELDS
	faculty_id = models.CharField(max_length=20,primary_key=True)
	prefix = models.CharField(max_length=20,default='Dr.')
	person = models.ForeignKey(Person,on_delete=models.CASCADE)
	gender = models.ForeignKey(Gender,on_delete=models.CASCADE)
	active = models.BooleanField(default = True)
	interest = models.CharField(max_length=200)
	department = models.ForeignKey(Department,on_delete=models.CASCADE)
	photograph = models.ImageField(upload_to = 'media/images/faculties/')

	# META CLASS
	class Meta:
		verbose_name = 'Faculty'
		verbose_name_plural = 'Faculties'

	# TO STRING METHOD
	def __str__(self):
		return (self.person.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class FacultyCourse(models.Model):

	# DATABASE FIELDS
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	cordinator = models.ForeignKey(Faculty,on_delete=models.CASCADE,related_name='cordinator')
	faculty = models.ForeignKey(Faculty,on_delete=models.CASCADE,related_name='faculty')
	session = models.ForeignKey(Session,on_delete=models.CASCADE)
	section = models.ForeignKey(Section,on_delete=models.CASCADE)


	# META CLASS
	class Meta:
		verbose_name = 'Faculty Course'
		verbose_name_plural = 'Faculty Courses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.course) + "  F :- " + str(self.faculty) + "  C :- " + str(self.cordinator) + "  S :- " + str(self.section)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something