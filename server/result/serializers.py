from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.models import *
from course.serializers import*
from result.models import*

class ComponentResultSerializer(ModelSerializer):


	class Meta:
		model = ComponentResult
		fields = "__all__"

class CourseResultSerializer(ModelSerializer):

	t1 = ComponentResultSerializer(read_only=True)
	mt = ComponentResultSerializer(read_only=True)
	t2 = ComponentResultSerializer(read_only=True)
	et = ComponentResultSerializer(read_only=True)


	class Meta:
		model = CourseResult
		fields = "__all__"

