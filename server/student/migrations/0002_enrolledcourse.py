# Generated by Django 2.0.3 on 2019-02-14 14:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0001_initial'),
        ('result', '0001_initial'),
        ('utility', '0002_person_email'),
        ('student', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EnrolledCourse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='course.Course')),
                ('course_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='course.CourseType')),
                ('grade', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Grade')),
                ('result', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='result.CourseResult')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Session')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.Student')),
            ],
            options={
                'verbose_name': 'Enrolled Course',
                'verbose_name_plural': 'Enrolled Courses',
            },
        ),
    ]
