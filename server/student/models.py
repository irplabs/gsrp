from django.db import models
from django.contrib.auth.models import User
from utility.models import*
from course.models import*


# Create your models here.

class Student(models.Model):

	# DATABASE FIELDS
	admission_id = models.CharField(max_length=20,primary_key=True)
	admission_date = models.DateField() 
	person = models.ForeignKey(Person,on_delete=models.CASCADE)
	address = models.ForeignKey(Address,on_delete=models.CASCADE,blank=True,null=True)
	guardian = models.ForeignKey(Guardian,on_delete=models.CASCADE,blank=True,null=True)
	local_guardian = models.ForeignKey(LocalGuardian,on_delete=models.CASCADE,blank=True,null=True)
	gender = models.ForeignKey(Gender,on_delete=models.CASCADE)
	category = models.ForeignKey(Category,on_delete=models.CASCADE)
	birth_date = models.DateField()
	aadhar = models.CharField(max_length=20,blank=True)
	grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
	section = models.ForeignKey(Section, on_delete=models.CASCADE)
	active = models.BooleanField(default = True)
	photograph = models.ImageField(upload_to = 'media/images/students/')

	# META CLASS
	class Meta:
		verbose_name = 'Student'
		verbose_name_plural = 'Students'

	# TO STRING METHOD
	def __str__(self):
		return (self.admission_id) + "-" + (self.person.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class EnrolledCourse(models.Model):
	student = models.ForeignKey(Student,on_delete=models.CASCADE)
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	course_type = models.ForeignKey(CourseType,on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
	result = models.ForeignKey(CourseResult, on_delete=models.CASCADE)
	date = models.DateField()
	# META CLASS
	class Meta:
		verbose_name = 'Enrolled Course'
		verbose_name_plural = 'Enrolled Courses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.student) + "-" + str(self.course) + "-" +str(self.result)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something