# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.views import*
from utility.authenticate import*
from utility.session import*
from fee_payment.models import*
from fee_payment.serializers import*

class FeeTypeListView(ListAPIView):
	queryset = FeeType.objects.all()
	serializer_class = FeeTypeSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = FeeTypeSerializer(queryset,many=True)
		return Response(serializer.data)



class FeeListView(ListAPIView):
	queryset = Fee.objects.all()
	serializer_class = FeeSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = FeeSerializer(queryset,many=True)
		return Response(serializer.data)


class PaymentListView(ListAPIView):
	queryset = Payment.objects.all()
	serializer_class = PaymentSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = PaymentSerializer(queryset,many=True)
		return Response(serializer.data)

class GatewayListView(ListAPIView):
	queryset = Gateway.objects.all()
	serializer_class = GatewaySerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GatewaySerializer(queryset,many=True)
		return Response(serializer.data)
