from django.db import models
from django.contrib.auth.models import User
from utility.models import*
# Create your models here.


class FeeType(models.Model):
	type_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	# META CLASS
	class Meta:
		verbose_name = 'FeeType'
		verbose_name_plural = 'FeeTypes'

	# TO STRING METHOD
	def __str__(self):
		return (self.type_id) + "-" + (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something	


class Fee(models.Model):
	fee_type = models.ForeignKey(FeeType,on_delete=models.CASCADE)
	grade = models.ForeignKey(Grade,on_delete=models.CASCADE)
	amount = models.DecimalField(max_digits=15,decimal_places=2,blank=True,default=Decimal('0.00'))
    # META CLASS
	class Meta:
		verbose_name = 'Fee'
		verbose_name_plural = 'Fees'

	# TO STRING METHOD
	def __str__(self):
		return  str(self.fee_type) +  "-" + str(self.grade) + "-" + str(self.amount)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class Gateway(models.Model):
	gateway_id =  models.CharField(max_length=50,primary_key=True)
	name = models.CharField(max_length=100)
	    # META CLASS
	class Meta:
		verbose_name = 'Gateway'
		verbose_name_plural = 'Gateways'

	# TO STRING METHOD
	def __str__(self):
		return  str(self.gateway_id) +  "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something 


class Payment(models.Model):
	pay_id = models.CharField(max_length=400,primary_key=True)
	payer = models.ForeignKey(Person,on_delete=models.CASCADE, related_name="payer")
	payee = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="payee")
	fee = models.ForeignKey(Fee,on_delete=models.CASCADE)
	verified_by =  models.ForeignKey(Person,on_delete=models.CASCADE,related_name="verified_by")
	verified_on = models.DateField()
	txid = models.CharField(max_length=100)
	gateway = models.ForeignKey(Gateway,on_delete=models.CASCADE)
	pay_date = models.DateField()
	session = models.ForeignKey(Session,on_delete=models.CASCADE)
	# META CLASS
	class Meta:
		verbose_name = 'Payment'
		verbose_name_plural = 'Payments'

	# TO STRING METHOD
	def __str__(self):
		return  str(self.payer) +  "-" + str(self.payee) + '-' +str(self.fee)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something 

	
	







