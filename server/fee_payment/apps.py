from django.apps import AppConfig


class FeePaymentConfig(AppConfig):
    name = 'fee_payment'
