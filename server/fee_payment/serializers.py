from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from fee_payment.models import*

class FeeTypeSerializer(ModelSerializer):

	class Meta:
		model = FeeType
		fields = "__all__"

class FeeSerializer(ModelSerializer):

	fee_type = FeeTypeSerializer(read_only=True)
	grade = GradeSerializer(read_only=True)

	class Meta:
		model = Fee
		fields = "__all__"

class GatewaySerializer(ModelSerializer):

	class Meta:
		model = Gateway
		fields = "__all__"

class PaymentSerializer(ModelSerializer):
    payer = PersonSerializer(read_only=True)
    payee = PersonSerializer(read_only=True)
    fee = FeeSerializer(read_only=True)
    verified_by = PersonSerializer(read_only=True)
    gateway = GatewaySerializer(read_only=True)
    session = SessionSerializer(read_only=True)
    
    class Meta:
        model = Payment
        fields = "__all__"
