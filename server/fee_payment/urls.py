from django.conf.urls import url
from django.urls import path, include
from fee_payment.views import*

urlpatterns = [

	
    url(r'^api/fee_types/$',FeeTypeListView.as_view(),name='view-all'),
    url(r'^api/fees/$',FeeListView.as_view(),name='view-all'),
    url(r'^api/payments/$',PaymentListView.as_view(),name='view-all'),
    url(r'^api/gateways/$',GatewayListView.as_view(),name='view-all'),




]