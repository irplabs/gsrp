# Generated by Django 2.0.3 on 2019-02-14 19:19

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('utility', '0002_person_email'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(blank=True, decimal_places=2, default=Decimal('0.00'), max_digits=15)),
            ],
            options={
                'verbose_name': 'Fee',
                'verbose_name_plural': 'Fees',
            },
        ),
        migrations.CreateModel(
            name='FeeType',
            fields=[
                ('type_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'FeeType',
                'verbose_name_plural': 'FeeTypes',
            },
        ),
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('gateway_id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Gateway',
                'verbose_name_plural': 'Gateways',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('pay_id', models.CharField(max_length=400, primary_key=True, serialize=False)),
                ('verified_on', models.DateField()),
                ('txid', models.CharField(max_length=100)),
                ('pay_date', models.DateField()),
                ('fee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fee_payment.Fee')),
                ('gateway', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fee_payment.Gateway')),
                ('payee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payee', to='utility.Person')),
                ('payer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payer', to='utility.Person')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Session')),
                ('verified_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='verified_by', to='utility.Person')),
            ],
            options={
                'verbose_name': 'Payment',
                'verbose_name_plural': 'Payments',
            },
        ),
        migrations.AddField(
            model_name='fee',
            name='fee_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fee_payment.FeeType'),
        ),
        migrations.AddField(
            model_name='fee',
            name='grade',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Grade'),
        ),
    ]
