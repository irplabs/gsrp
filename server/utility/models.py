# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from datetime import date
from decimal import Decimal

# Create your models here.

class Department(models.Model):

	# DATABASE FIELDS
	department_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)


	# META CLASS
	class Meta:
		verbose_name = 'Department'
		verbose_name_plural = 'Departments'

	# TO STRING METHOD
	def __str__(self):
		return (self.department_id) + "-" + (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

	# ABSOLUTE URL METHOD
	def get_absolute_url(self):
		return reverse('department_details', kwargs={'pk': self.department_id})

# Create your models here.

class Session(models.Model):

	# DATABASE FIELDS
	session_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	start_date  = models.DateField(default=date.today)
	end_date = models.DateField(default=date.today)

	# META CLASS
	class Meta:
		verbose_name = 'Session'
		verbose_name_plural = 'Sessions'

	# TO STRING METHOD
	def __str__(self):
		return (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something




class Category(models.Model):

	# DATABASE FIELDS
	category_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)
	

	# META CLASS
	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Categories'

	# TO STRING METHOD
	def __str__(self):
		return (self.category_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something



class Address(models.Model):

	# DATABASE FIELDS
	house_address = models.TextField(max_length=500)
	pincode = models.CharField(max_length=20)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=50)
	country = models.CharField(max_length=50)
	

	# META CLASS
	class Meta:
		verbose_name = 'Address'
		verbose_name_plural = 'Addresses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.city) + "-" + str(self.state)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class Person(models.Model):

	# DATABASE FIELDS
	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True,null=True)
	name = models.CharField(max_length=200)
	phone = models.CharField(max_length=20)
	email = models.EmailField(max_length=70,blank=True,unique=True)

	# META CLASS
	class Meta:
		verbose_name = 'Person'
		verbose_name_plural = 'Persons'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name) + "-" + str(self.phone)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something



class Guardian(models.Model):

	# DATABASE FIELDS
	father = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="father")
	mother = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="mother")

	
	# META CLASS
	class Meta:
		verbose_name = 'Guardian'
		verbose_name_plural = 'Guardians'

	# TO STRING METHOD
	def __str__(self):
		return str(self.father) + "-" + str(self.mother)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class LocalGuardian(models.Model):

	# DATABASE FIELDS
	person = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="local_guardian")
	address = models.ForeignKey(Address,on_delete=models.CASCADE)

	
	# META CLASS
	class Meta:
		verbose_name = 'Local Guardian'
		verbose_name_plural = 'Local Guardians'

	# TO STRING METHOD
	def __str__(self):
		return str(self.person) + "-" + str(self.address)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.



class Gender(models.Model):

	# DATABASE FIELDS
	gender_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Gender'
		verbose_name_plural = 'Genders'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class Grade(models.Model):

	# DATABASE FIELDS
	grade_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Grade'
		verbose_name_plural = 'Grades'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class Section(models.Model):

	# DATABASE FIELDS
	section = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Section'
		verbose_name_plural = 'Sections'

	# TO STRING METHOD
	def __str__(self):
		return str(self.section)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

