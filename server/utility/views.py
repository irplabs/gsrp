# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.authenticate import*
from utility.session import*
from server.settings import SECRET_KEY
import jwt

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
    	try:
    		serializer = self.serializer_class(data=request.data,context={'request': request })
    		serializer.is_valid(raise_exception=True)

    		user = serializer.validated_data['user']
    		ip = get_client_ip(request)
    		jwt_token = jwt.encode({ 'username': user.username,'ip': ip },SECRET_KEY,algorithm = 'HS256').decode('utf-8')

    		group = Group.objects.get(user=user)
    		return JsonResponse({ 'status': 'LOGIN_SUCCESS', 'token': jwt_token, 'group': group.name })

    	except Exception as e:
    		print(str(e))
    		return Response({ 'status': 'LOGIN_FAILED' })

class DepartmentListView(ListAPIView):
	queryset = Department.objects.all()
	serializer_class = DepartmentSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = DepartmentSerializer(queryset,many=True)
		return Response(serializer.data)


class SessionListView(ListAPIView):
	queryset = Session.objects.all()
	serializer_class = SessionSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SessionSerializer(queryset,many=True)
		return Response(serializer.data)

class SessionDetailView(RetrieveAPIView):
	queryset = Session.objects.all()
	serializer_class = SessionSerializer

class CategoryListView(ListAPIView):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = CategorySerializer(queryset,many=True)
		return Response(serializer.data)


class AddressListView(ListAPIView):
	queryset = Address.objects.all()
	serializer_class = AddressSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = AddressSerializer(queryset,many=True)
		return Response(serializer.data)

class PersonListView(ListAPIView):
	queryset = Person.objects.all()
	serializer_class = PersonSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = PersonSerializer(queryset,many=True)
		return Response(serializer.data)

class GuardianListView(ListAPIView):
	queryset = Guardian.objects.all()
	serializer_class = GuardianSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GuardianSerializer(queryset,many=True)
		return Response(serializer.data)


class LocalGuardianListView(ListAPIView):
	queryset = LocalGuardian.objects.all()
	serializer_class = LocalGuardianSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = LocalGuardianSerializer(queryset,many=True)
		return Response(serializer.data)


class GenderListView(ListAPIView):
	queryset = Gender.objects.all()
	serializer_class = GenderSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GenderSerializer(queryset,many=True)
		return Response(serializer.data)

class GradeListView(ListAPIView):
	queryset = Grade.objects.all()
	serializer_class = GradeSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GradeSerializer(queryset,many=True)
		return Response(serializer.data)

class SectionListView(ListAPIView):
	queryset = Section.objects.all()
	serializer_class = SectionSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SectionSerializer(queryset,many=True)
		return Response(serializer.data)
