from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from utility.models import*
from rest_framework import*

class DepartmentSerializer(ModelSerializer):

	class Meta:
		model = Department
		fields = "__all__"


class SessionSerializer(ModelSerializer):

	class Meta:
		model = Session
		fields = "__all__"


class CategorySerializer(ModelSerializer):

	class Meta:
		model = Category
		fields = "__all__"	
	

class AddressSerializer(ModelSerializer):

	class Meta:
		model = Address
		fields = "__all__"	

class PersonSerializer(ModelSerializer):

	class Meta:
		model = Person
		fields = "__all__"	


class GuardianSerializer(ModelSerializer):

	father = PersonSerializer(read_only=True)
	mother = PersonSerializer(read_only=True)

	class Meta:
		model = Guardian
		fields = "__all__"

class LocalGuardianSerializer(ModelSerializer):

	person = PersonSerializer(read_only=True)
	address = AddressSerializer(read_only=True)

	class Meta:
		model = LocalGuardian
		fields = "__all__"

class GenderSerializer(ModelSerializer):

	class Meta:
		model = Gender
		fields = "__all__"

class GradeSerializer(ModelSerializer):

	class Meta:
		model = Grade
		fields = "__all__"

class SectionSerializer(ModelSerializer):

	class Meta:
		model = Section
		fields = "__all__"

