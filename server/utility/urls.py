from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from utility.views import CustomAuthToken
from utility.views import*


urlpatterns = [

    url(r'^api-token-auth/', CustomAuthToken.as_view()),
    url(r'^api/departments/',DepartmentListView.as_view(),name='view-all'),
    url(r'^api/sessions/$',SessionListView.as_view(),name='view-all'),
    url(r'^api/sessions/(?P<pk>[\w\-]+)',SessionDetailView.as_view(),name='details'),
    url(r'^api/categories/',CategoryListView.as_view(),name='view-all'),
    url(r'^api/addresses/',AddressListView.as_view(),name='view-all'),
    url(r'^api/persons/',PersonListView.as_view(),name='view-all'),
    url(r'^api/guardians/',GuardianListView.as_view(),name='view-all'),
    url(r'^api/local_guardians/',LocalGuardianListView.as_view(),name='view-all'),
    url(r'^api/genders/',GenderListView.as_view(),name='view-all'),
    url(r'^api/grades/',GradeListView.as_view(),name='view-all'),
    url(r'^api/sections/',SectionListView.as_view(),name='view-all'),

]