from django.contrib.auth.models import Group,User
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from server.settings import SECRET_KEY

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return (ip)

def isAuthenticated(request,access_group):

	try:
		cookie = request.get_signed_cookie('jwt')
	except Exception as e:
		print(str(e))
		return 'SESSION_EXPIRED'

	ip = get_client_ip(request)
	SECRET = settings.SECRET_KEY
	jwt_data = jwt.decode(cookie, SECRET_KEY, algorithm = 'HS256')
	username = jwt_data['username']
	jwt_ip = jwt_data['ip']

	try:
		group = Group.objects.get(user=user)
		if ip != jwt_ip or group.name not in access_group:
			return 'AUTHORIZATION_FAILED'
	except Exception as e:
		print(str(e))
		return 'AUTHORIZATION_FAILED'

	return 'SUCCESS'