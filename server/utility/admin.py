# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import*
from import_export import resources


# Register your models here.

@admin.register(Department)
class DepartmentAdmin(ImportExportModelAdmin):
    pass

@admin.register(Session)
class SessionAdmin(ImportExportModelAdmin):
	pass

@admin.register(Category)
class CategoryAdmin(ImportExportModelAdmin):
	pass

@admin.register(Address)
class AddressAdmin(ImportExportModelAdmin):
	pass

@admin.register(Person)
class PersonAdmin(ImportExportModelAdmin):
	pass

@admin.register(Guardian)
class GuardianAdmin(ImportExportModelAdmin):
	pass

@admin.register(LocalGuardian)
class LocalGuardianAdmin(ImportExportModelAdmin):
	pass

@admin.register(Gender)
class GenderAdmin(ImportExportModelAdmin):
	pass

@admin.register(Grade)
class GradeAdmin(ImportExportModelAdmin):
	pass

@admin.register(Section)
class SectionAdmin(ImportExportModelAdmin):
	pass
