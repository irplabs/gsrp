from django.conf.urls import url
from django.urls import path, include
from course.views import*

urlpatterns = [

	
    url(r'^api/courses/$',CourseListView.as_view(),name='view-all'),
    url(r'^api/courses/(?P<pk>[\w\-]+)',CourseDetailView.as_view(),name='detail'),

]