from django.db import models
from datetime import date
from utility.models import *
from result.models import *



# Create your models here.
class Course(models.Model):

	# DATABASE FIELDS
	course_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	max_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True,null=True)
	department = models.ForeignKey(Department,on_delete=models.CASCADE)


	# META CLASS
	class Meta:
		verbose_name = 'Course'
		verbose_name_plural = 'Courses'

	# TO STRING METHOD
	def __str__(self):
		return (self.course_id) + "-" + (self.name) + "-" + str(self.department.department_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

# Create your models here.
class CourseType(models.Model):

	# DATABASE FIELDS
	type_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	# META CLASS
	class Meta:
		verbose_name = 'Course Type'
		verbose_name_plural = 'Course Types'

	# TO STRING METHOD
	def __str__(self):
		return  (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

