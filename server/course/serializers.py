from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.models import *

class CourseSerializer(ModelSerializer):

	department = DepartmentSerializer(read_only=True)

	class Meta:
		model = Course
		fields = "__all__"

class CourseTypeSerializer(ModelSerializer):

	class Meta:
		model = CourseType
		fields = "__all__"
