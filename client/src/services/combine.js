/* The objective of the function is to assign localStorage values to reducers.
In case a reducer is yet to be loaded (dynamic reducers), the reducer key is assigned null.
Without this function, if we directly use combineReducers it will drop the reducer keys.
e.g.
    initialState = { authentication: ...., ui: ...., admin: ....}    admin is dynamically loaded
    reducer = { authentication, ui }

    combineReducers will not get admin key in reducer, and will drop the reducer.
*/

import { combineReducers } from 'redux';

export default (reducers) => {
    const initialState = localStorage['data'] ? JSON.parse(localStorage['data']) : {};
    const reducerNames = Object.keys(reducers);

    Object.keys(initialState).forEach(item => {
        if (reducerNames.indexOf(item) === -1) {
            reducers[item] = (state = null) => state;
        }
    });
    
    return combineReducers(reducers);
}