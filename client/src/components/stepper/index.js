import React, { Component } from 'react';
import {
    Step,
    StepButton,
    Stepper,
    withStyles,
    Typography,
    Paper,
    Fab
} from '@material-ui/core';
import {
    NavigateBefore as PrevIcon,
    NavigateNext as NextIcon,
} from '@material-ui/icons';

import styles from './styles';

class CustomStepper extends Component {
    constructor(props) {
        super(props);

        this.state = { step: 0, completed: {} };
    }

    changeStep = (step) => {
        const { step: oldStep, completed } = this.state;

        this.setState({
            step,
            completed: {
                ...completed,
                [oldStep]: true
            }
        });
    }

    render() {
        const { step, completed } = this.state;
        const { steps, classes } = this.props;

        return (
            <div className={classes.root}>
                <Stepper nonLinear orientation="vertical" activeStep={step} className={classes.stepper}>
                    {steps.map((each,index) => {
                        const props = {};
                        const buttonProps = {};

                        if (each.optional)
                            buttonProps.optional = <Typography variant="caption">Optional</Typography>;
                        return (
                            <Step key={index}>
                                <StepButton
                                    onClick={() => this.changeStep(index)}
                                    completed={completed[index]}
                                    { ...buttonProps }
                                >
                                    {each.label}
                                </StepButton>
                            </Step>
                        );
                    })}
                </Stepper>
                <div className={classes.content}>
                    <div className={classes.container}>
                        <Paper className={classes.paper}>
                            { steps[step].content }
                        </Paper>
                    </div>
                    <div className={classes.buttonContainer}>
                        {step!=0 && 
                            <Fab className={classes.leftButton} onClick={() => this.changeStep(step - 1)}>
                                <PrevIcon />
                            </Fab>
                        }
                        {step!=steps.length-1 && 
                            <Fab className={classes.rightButton} onClick={() => this.changeStep(step + 1)}>
                                <NextIcon />
                            </Fab>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(CustomStepper);