export default (theme) => ({
    root: {
        display: 'flex',
    },
    stepper: {
        flex: 1,
    },
    content: {
        flex: 4,
        display: 'flex',
        flexDirection: 'column',
    },
    container: {
        flex: 3,
        display: 'flex',
    },
    paper: {
        margin: 'auto',
        height: '80%',
        width: '80%',
    },
    buttonContainer: {
        flex: 1
    },
    leftButton: {
        float: 'left',
        left: '3%',
    },
    rightButton: {
        float: 'right',
        right: '3%',
    },
})