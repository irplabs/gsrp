/**
 * Since the extra-baggage code associated with an EditableCell is unrelated to Table logic, it is moved to a separate component i.e. CustomCell
 */

/**
 * TODO: + Find a way to highlight the box when editing.
 */
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles, TableCell } from '@material-ui/core';
import styles from './styles';

const ENTER_KEY = 13;

class CustomCell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isEditing: false,
        };
    }

    handleDoubleClick = (event) => {
        var node = event.target;

        node.contentEditable = true;

        node.focus();

        this.setState({
            isEditing: true,
        });
    }   

    nodeCleanupAndUpdate = (node) => {
        node.contentEditable = false;
        
        this.setState({
            isEditing: false,
        });

        var value = node.textContent.trim();

        this.props.updateCell(this.props.rowInd, this.props.cellInd, value);
    }

    handleEnterKeyDown = (event) => {
        if(event.which === ENTER_KEY) {
            event.preventDefault();
            
            var node = event.target;
            
            this.nodeCleanupAndUpdate(node);
        }
    }

    handleBlur = (event) => {
        var node = event.target;
        
        this.nodeCleanupAndUpdate(node);
    }

    render() {
        const {
            canEdit,
            rowInd,
            cellInd,
            updateCell,
            className,
            children,
            classes,
            ...restProps
        } = this.props;     

        let customProps = {};

        if(canEdit) {
            customProps = {
                className : classnames(
                    className,
                    this.state.isEditing && classes.editing,
                ),
                suppressContentEditableWarning: true,
                onDoubleClick: this.handleDoubleClick,
                onKeyDown: this.handleEnterKeyDown,
                onBlur: this.handleBlur,
            }
        }

        return (
            <TableCell
                {...customProps}
                {...restProps}
            >   
                {children}
            </TableCell>
        )
    }
}

CustomCell.propTypes = {
    canEdit: PropTypes.bool.isRequired,
    rowInd: PropTypes.number.isRequired,
    cellInd: PropTypes.number.isRequired,
    updateCell: PropTypes.func.isRequired,
    className: PropTypes.object,
    children: PropTypes.node.isRequired,
    classes: PropTypes.object,
};

export default withStyles(styles)(CustomCell);