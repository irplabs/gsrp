
export default (theme) => ({
    editing: {
        position: 'relative',
        fontWeight: 'bold',
        fontSize: 14,
        transition: 'font-size 0.15s, font-weight 0.15s',
    },
});