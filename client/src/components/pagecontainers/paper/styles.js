export default (theme) => ({
    root: {
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'column',
        paddingBottom: `${4*theme.spacing.unit}px`,
    },
    headerRoot: {
        width: '100%',
        height: '20vh',
        minHeight: '120px',
        backgroundColor: theme.palette.primary.main,
        padding: `${4*theme.spacing.unit}px`,
    },
    headerTitle: {
        color: "#FFF",
    },
    paperRoot: {
        width: `calc(100% - ${8*theme.spacing.unit}px)`,
        padding: `${2*theme.spacing.unit}px`,
        marginLeft: `${4*theme.spacing.unit}px`, // Shift the content to the center.
        marginTop: '-64px',    
        minHeight: `calc(100vh - 20vh - ${2*theme.spacing.unit}px)`,

        '& > *': {
            marginTop: 2*theme.spacing.unit,
            marginBottom: 2*theme.spacing.unit,
        },
    },
});