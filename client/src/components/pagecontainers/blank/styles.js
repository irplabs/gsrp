export default (theme) => ({
    root: {
        display: 'flex',
        padding: 2*theme.spacing.unit,
        flexGrow: 2,

        '& > *': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
        },
    },
    loadingContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 2,
    },
    loadingRoot: {
        width: '40%',
    },
    loadingMessage: {
        margin: `${2*theme.spacing.unit}px auto`,
        textTransform: 'uppercase',
    },
});