import React, { Component, Fragment } from 'react';

import { Table, Stepper } from '../../../components';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [
                { link: '/haha', data: [1,'Maharshi'] },
                { link: '/haha', data: [2, 'Yogesh'] },
                { data: [3, 'Jatin'] },
                { data: [4, 'Gangadhara'] },
            ],
            columns: [
                { type: 'number', name: 'ID', filter: true, edit: true, sort: true },
                { type: 'string', name: 'Name', edit: true, filter: true, sort: true }
            ]
        };
    }

    render() {
        const { rows, columns } = this.state;
        return (
            <Fragment>
                <Table rows={rows} columns={columns} />
                {/* <div style={{ height: 40 }}></div>
                <Stepper
                    steps={[
                        { label: 'Step 1', optional: false, content: 'Maharshi Roy' }, 
                        { label: 'Step 2', optional: true, content: 'Yogesh Gupta' }, 
                        { label: 'Step 3', optional: false, content: 'Jatin Goel' }, 
                        { label: 'Step 4', optional: false, content: 'Tara Prasad Tripathy' }
                    ]}
                /> */}
            </Fragment>
        );
    }
}

export default Home;