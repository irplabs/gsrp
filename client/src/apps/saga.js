import { take, call } from 'redux-saga/effects';

import authentication from './authentication';

export default function* root() {
    while (true) {
        yield call(authentication.sagas.loginFlow);
    }
}