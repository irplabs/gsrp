import C from './actionTypes';
import { combineReducers } from 'redux';

const username = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.SUCCESS:
            return action.data.username;
        default:
            return state;
    }
}

const token = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.SUCCESS:
            return action.data.token;
        default:
            return state;
    }
}

const group = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.SUCCESS:
            return action.data.group;
        default:
            return state;
    }
}

export default combineReducers({
    username,
    token,
    group,
});