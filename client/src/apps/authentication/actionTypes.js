export default {
    LOGIN: {
        INITIATE: "INITIATE",    //initiate request
        SUCCESS: "SUCCESS",      //successful login
    },
    LOGOUT: {
        INITIATE: "INITIATE",   //initiate logout
        SUCCESS: "SUCCESS"      //successful logout
    }
};