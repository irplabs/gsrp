import React, { Fragment } from 'react';
import { ConnectedRouter } from 'connected-react-router'
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import Routing from './routing';
import { history } from '../services';

import CssBaseline from '@material-ui/core/CssBaseline';
import ui from './ui';

const mapStateToProps = (state, props) => ({});

const mapDispatchToProps = dispatch => ({});

const LocationBlocker = () =>     //Check out the reason for creating LocationBlocker here: [https://github.com/facebook/react/issues/9214#issuecomment-353161691]
    <ui.ThemeProvider>
        <Fragment>
            {/* CssBaseline is a Material-UI component that fixes some inconsistencies across browsers and devices while providing slightly more opinionated resets to common HTML elements. For more visit : https://material-ui.com/style/css-baseline/ */}
            <CssBaseline />

            <Routing />
        </Fragment>
    </ui.ThemeProvider>

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: ''
        }
    }

    render() {
        return (
            <ConnectedRouter history={history}>
                <Route component={LocationBlocker} />
            </ConnectedRouter>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
    