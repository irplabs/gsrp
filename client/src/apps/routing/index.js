import React from 'react';
import { Switch, Redirect, Route } from 'react-router';

import CustomRoute from './CustomRoute';
import home from '../home';
import authentication from '../authentication';
import admin from '../admin';

export default () => (
    <Switch>
        <CustomRoute exact path="/" component={() => <home.Index /> } layout={home.layoutConfig} />
        <CustomRoute path="/login" component={() => <authentication.Index />} layout={authentication.layoutConfig} />
        <CustomRoute allowedGroup='ADMIN' path="/admin" component={() => <admin.Index />} layout={admin.layoutConfig} />
        <Route render={() => <Redirect to="/login" />} />
    </Switch>
);