import React from 'react';
import { connect } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core';
import { light, dark } from './themes';
import { THEME_TYPES } from '../constants';

const mapStateToProps = (state, props) => ({
    theme: state.ui.theme
});

export default connect(mapStateToProps)(({ theme, children }) => {
    return (
        <MuiThemeProvider theme={(
            (theme===THEME_TYPES.LIGHT && light) || 
            (theme===THEME_TYPES.DARK && dark)
        )}>
            {children}
        </MuiThemeProvider>
    )
})