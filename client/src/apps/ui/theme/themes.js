import { createMuiTheme } from '@material-ui/core';
import { blue, red } from '@material-ui/core/colors';

export const light = createMuiTheme({
    palette: {
        primary: {
            main: '#3C4252',
            light: '#7D818C',
            dark: '#1E2129',
            contrastText: "#FFF",
        },
        secondary: {
            main: '#039BE5',
        },
        background: {
            appBar: '#f5f5f5',
        },
    },
    typography: {
        useNextVariants: true,
    }
})

export const dark = createMuiTheme({
    palette: {
        primary: red,
    },
    typography: {
        useNextVariants: true,
    }
})