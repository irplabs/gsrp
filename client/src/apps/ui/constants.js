export const NAME = 'ui';

export const THEME_TYPES = {
    LIGHT: "LIGHT",
    DARK: "DARK"
};