import React from 'react';
import { connect } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import { IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';

import PageLoader from './page-loader';
import ToastManager from './toast-manager';

const mapStateToProps = (state, props) => ({
    loading: state.ui.loading,
    toast: state.ui.toast,
});

const Wrapper = connect(mapStateToProps)(({ loading, toast, children }) => (
    <div style={{ position: 'relative '}}>
        {loading && <PageLoader />}
        <ToastManager toast={toast} />
        {children}
    </div>
));

export default ({ ...props }) => 
    <SnackbarProvider
        maxSnack={3}
        action={[
            //key ought to be added to IconButton because of some warning. 
            //Please suggest a proper way other than this workaround
            <IconButton key="close" style={{/* Insert Button Styles here */}}>
                <CloseIcon style={{ fontSize: 20 }} />
            </IconButton>
        ]}
    >
        <Wrapper {...props} />
    </SnackbarProvider>
