import React, { Fragment } from 'react';
import { withStyles, LinearProgress, Fade } from '@material-ui/core';

import styles from './styles';

export default withStyles(styles)(({ classes }) =>
    <Fragment>
        <LinearProgress classes={{ 
            root: classes.linearIndeterminateRoot,
        }}/>
        <Fade in={true} timeout={10}>
            <div className={classes.overlay}></div>
        </Fade>
    </Fragment>
);