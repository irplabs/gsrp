import React, { Component } from 'react';
import { withSnackbar } from 'notistack';

class ToastManager extends Component {

    shouldComponentUpdate({ toast: newToast }) {
        const { toast: currentToast } = this.props;
        return (newToast!=null && (currentToast==null || newToast.key != currentToast.key));
        //null check is required because first time reducer value of toast is initialized with null
        //so key will not be an attribute for currentToast and will throw error
    }

    componentDidUpdate() {
        const { toast, enqueueSnackbar } = this.props;
        enqueueSnackbar(toast.message, { variant: toast.variant, autoHideDuration: 2000 });
    }

    render() { return null; }
}

export default withSnackbar(ToastManager);