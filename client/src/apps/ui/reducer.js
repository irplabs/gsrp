import C from './actionTypes';
import { THEME_TYPES } from './constants';
import { combineReducers } from 'redux';

const theme = (state=THEME_TYPES.LIGHT, action) => {
    switch(action.type) {
        case C.CHANGE_THEME:
            return action.payload;
        default:
            return state;
    }
}

const toast = (state=null, action) => {
    switch(action.type) {
        case C.ADD_TOAST:
            return action.payload;
        default:
            return state;
    }
}

const loading = (state=false, action) => {
    switch(action.type) {
        case C.TOGGLE_LOADING:
            return !state;
        default:
            return state;
    }
}

export default combineReducers({
    theme,
    toast,
    loading,
});