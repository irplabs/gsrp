export default (theme) => ({
    root: {},
    nested: {
        '& > a, & > li': {
            textIndent: 2*theme.spacing.unit,
        },
        
        '& > a > svg, & > li > svg': {
            marginLeft: 2*theme.spacing.unit,
        },
    },
});