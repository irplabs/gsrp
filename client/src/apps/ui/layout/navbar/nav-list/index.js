import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles, List } from '@material-ui/core';
import NestedListWrapper from './nested-list-wrapper';
import NavListItem from './nav-list-item';
import styles from './styles';

const NavList = ({ nested, list, toggle, classes, ...props}) => {
    return (
        <List className={classnames(
            classes.root,
            nested && classes.nested)}>
            {list.map((item, index) => 
                item.nesting ? 
                    <NestedListWrapper key={index} item={item} toggle={toggle} /> :
                    <NavListItem key={index} variant="link" title={item.title} url={item.url} icon={item.icon} onClick={toggle()} />)}
        </List>
    )
};

NavList.propTypes = {
    nested: PropTypes.bool,
    list: PropTypes.array.isRequired,
    toggle: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(NavList);