import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { withStyles, ListItem, ListItemText } from '@material-ui/core';
import styles from './styles';

const NavListItem = ({ variant, title, url, icon, onClick, classes, ...props }) => {
    let componentProps = {};

    if(variant === "link") {
        componentProps = {
            component: NavLink,
            to: url,
            activeClassName: "active",
            exact: true,
        }
    }
    else { // variant === "item"
        componentProps.component = "li";
    }

    return (
        <ListItem 
            button
            {...componentProps}
            className={classes.root}
            onClick={onClick}
            {...props}
            >
            {icon}
            <ListItemText inset primary={title} classes={{ 
                primary: classes.primaryContent,
            }}/>
            {props.children}
        </ListItem>
    )
};

NavListItem.propTypes = {
    variant: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string,
    icon: PropTypes.node,
    onClick: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
}

NavListItem.defaultProps = {
    variant: "link",
}

export default withStyles(styles)(NavListItem);