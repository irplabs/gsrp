import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Collapse } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import NavList from '../index';
import NavListItem from '../nav-list-item';
import styles from './styles';

class NestedListWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    toggleCollapse = () => {
        this.setState({
            open: !this.state.open,
        });
    }

    render() {
        const { item, toggle, classes, ...restProps } = this.props;
        const { open } = this.state;

        return (
            <React.Fragment>
                <NavListItem variant="item" title={item.title} icon={item.icon} onClick={this.toggleCollapse}>
                    {open ? <ExpandLess /> : <ExpandMore />}
                </NavListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <NavList nested list={item.list} toggle={toggle} />
                </Collapse>
            </React.Fragment>
        )
    }
}

NestedListWrapper.propTypes = {
    item: PropTypes.object.isRequired,
    toggle: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(NestedListWrapper);