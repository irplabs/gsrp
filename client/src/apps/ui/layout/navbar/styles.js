export default (theme) => ({
    drawerRoot: {
        width: 280,
        height: 'auto',
        minHeight: '100vh',
        '-webkit-overflow-scrolling': 'touch',
        backgroundColor: theme.palette.primary.main,
        background: 'linear-gradient(rgba(0, 0, 0, 0) 30%, rgba(0, 0, 0, 0) 30%), linear-gradient(rgba(0, 0, 0, 0.25) 0, rgba(0, 0, 0, 0) 40%)',
    },
    siteInfoWrapper: {
        position: 'relative',
        padding: 0,
    },
    drawerCloseButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        width: 64,
        height: 64,
        color: '#FFF',
    },
    userInfoWrapper: {
        minHeight: 280,
    },
    navListWrapper: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: 'calc(100vh - 280px - 64px)',
        backgroundColor: '#424242',
    },
})