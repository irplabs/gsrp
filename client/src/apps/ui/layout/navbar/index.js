import React from 'react';
import { withStyles, SwipeableDrawer, IconButton, Typography, Toolbar } from '@material-ui/core';
import { Menu } from '@material-ui/icons';

import User from './user';
import NavList from './nav-list';
import styles from './styles';

/* REMAINDER: Below imports and config is for testing and development. */
import { Home, Dashboard, People, PeopleOutline, PersonAdd, } from '@material-ui/icons';

const list = [
    {
        url: '/',
        title: 'Home',
        icon: <Home />,
    },
    {
        url: '/admin/analytics',
        title: 'Analytics',
        icon: <Dashboard />,
    },
    {
        nesting: true,
        title: 'Users1',
        icon: <PeopleOutline />,
        list: [
            {
                url: '/admin/users',
                title: 'All Users1',
                icon: <People />,
            },
            {
                url: '/admin/users/add',
                title: 'Create User1',
                icon: <PersonAdd />
            },
            {
                nesting: true,
                title: 'Users3',
                icon: <PeopleOutline />,
                list: [
                    {
                        url: '/admin/users',
                        title: 'All Users3',
                        icon: <People />,
                    },
                    {
                        url: '/admin/users/add',
                        title: 'Create User3',
                        icon: <PersonAdd />,
                    },
                ]
            }
        ]
    },
]

export default withStyles(styles)(({ classes, open, toggle, navigation, ...props }) => 
    <SwipeableDrawer open={open} onOpen={toggle(true)} onClose={toggle(false)}>
        {/* The UI seen */}
        <div className={classes.drawerRoot}>
            
        
            {/* Some title, logo to be shown. */}
            <Toolbar className={classes.siteInfoWrapper}>
                
                {/* Close icon for navbar */}
                <IconButton className={classes.drawerCloseButton} onClick={toggle()}><Menu /></IconButton>
            
            </Toolbar>

            {/* User Information */}
            <div className={classes.userInfoWrapper}>
                <User />
            </div>

            {/* Navigation Links */}
            <div className={classes.navListWrapper}>
                <NavList list={list} toggle={toggle} />
            </div>
        </div>

    </SwipeableDrawer>
);