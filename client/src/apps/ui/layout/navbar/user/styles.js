export default (theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 2*theme.spacing.unit,
        color: theme.palette.primary.contrastText,

        '& > *': {
            margin: `${theme.spacing.unit}px auto`,
        },
    },
    image: {
        width: 120,
        height: 120,
    },
    roundedImage: {
        borderRadius: '50%',
    },
});