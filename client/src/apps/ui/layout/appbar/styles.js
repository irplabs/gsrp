export default (theme) => ({
    root: {
        backgroundColor: theme.palette.background.appBar,
    },
    toolbar: {
        display: 'flex'
    },
    leftPanel: {
        flex: 3
    },
    rightPanel: {
        flex: 1
    },
    navbarButton: {

    },
    notificationButton: {
        float: 'right'
    }
});