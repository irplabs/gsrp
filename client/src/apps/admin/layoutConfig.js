export default {
    navbar: {
        display: true,
    },
    appbar: {
        display: true,
        navbar: true,
        notifications: false,
    },
    notifications: {
        display: false,
    },
    footer: {
        display: false,
    },
};