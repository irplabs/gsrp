export default (theme) => ({
    form: {
        width: '80%',
        margin: '0 auto',

        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },

        '& > *': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
        }
    },
    formToolbar: {
        display: 'flex',
        justifyContent: 'flex-end',
        padding: 0,
    },
});