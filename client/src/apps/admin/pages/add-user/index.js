import React from 'react';
import { Containers } from '../../../../components';
import { withStyles, Grid, Toolbar, Divider, TextField, Button, Typography } from '@material-ui/core';
import styles from './styles';

class AddUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            form: {
                name: {
                    value: "",
                    errorMessage: "",
                },
                phone: {
                    value: "",
                    errorMessage: "",
                },
                group: {
                    value: "",
                    errorMessage: "",
                },
            },
        };
    }

    nameValidator = (value) => {
        return "";
    }

    handleChange = (attributeName, validator = (() => {})) => (event) => {
        this.setState({
            form: {
                ...this.state.form,
                [attributeName]: {
                    value: event.target.value,
                    errorMessage: validator(event.target.value),
                }
            }
        }, () => console.log(this.state));
    }

    render() {
        const {
            classes,
        } = this.props;

        const { 
            form,
        } = this.state;

        return (
            <Containers.Simple loading={this.state.loading} titleProps="User Registration">

                <form className={classes.form}>
                    <Toolbar className={classes.formToolbar}>
                        <Button variant="contained" color="primary">Upload File</Button>
                        <Button variant="contained" color="primary" type="submit">Submit</Button>
                    </Toolbar>

                    <Divider className={classes.formDivider} variant="middle" />

                    <TextField
                        className=""
                        label="Name"
                        id="name"
                        name="name"
                        value={form.name.value}
                        onChange={this.handleChange('name', this.nameValidator)}
                        error={Boolean(form.name.errorMessage)}
                        helperText={form.name.errorMessage}
                        type="text"
                        variant="outlined"
                        autoFocus
                        fullWidth
                    />

                    <TextField
                        className=""
                        label="Phone"
                        id="phone"
                        name="phone"
                        type="number"
                        value={form.phone.value}
                        onChange={this.handleChange('phone')}
                        error={Boolean(form.phone.errorMessage)}
                        variant="outlined"
                        fullWidth
                    />

                    <TextField
                        className=""
                        label="Group"
                        id="group"
                        name="group"
                        value={form.group.value}
                        onChange={this.handleChange}
                        type="text"
                        variant="outlined"
                        fullWidth
                    />
                </form>

            </Containers.Simple>
        );
    }
}

export default withStyles(styles)(AddUser);