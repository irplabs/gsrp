export default {
    menu: [
        { name: 'Getting Started', menu: ['Prerequisites', 'File Structure Explanation'] },
        { name: 'Apps', menu: ['Authentication', 'Routing']},
    ],
}